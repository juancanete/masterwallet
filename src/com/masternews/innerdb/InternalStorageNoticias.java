package com.masternews.innerdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class InternalStorageNoticias {
	
	public static final String KEY_ID = "_idNoticia";
	public static final String KEY_TITLE = "tituloNoticia";
	public static final String KEY_DESCRIPTION = "descripcionNoticia";
	public static final String KEY_IMAGE = "imagenNoticia";
	public static final String KEY_VOTES = "votosNoticia";
	public static final String KEY_USER = "usuarioNoticia";
	public static final String KEY_CATEGORY = "categoriaNoticia";

	private static final String DATABASE_NAME = "BDInterna";
	private static final String DATABASE_TABLE = "Noticia";
	private static final int DATABASE_VERSION = 1;
	
	private DbHelper ourHelper;
	private final Context ourContext;
	private SQLiteDatabase ourDatabase;
	
	private static class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" +
					KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					KEY_TITLE + " TEXT NOT NULL, " +
					KEY_DESCRIPTION + " TEXT NOT NULL, " + 
					KEY_IMAGE + " TEXT, " +
					KEY_VOTES + " INTEGER NOT NULL, " + 
					KEY_USER + " INTEGER NOT NULL," + 
					KEY_CATEGORY + " INTEGER NOT NULL);"
			);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}
		
	}
	
	public InternalStorageNoticias (Context c) {
		ourContext = c;
	}
	
	public InternalStorageNoticias open() throws SQLException {
		ourHelper = new DbHelper(ourContext);
		ourDatabase = ourHelper.getWritableDatabase();
		return this;
	}
	
	public void close(){
		ourHelper.close();
	}
	
	public long createEntry (String title, String description, String image, int votes, int user, int category) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_TITLE, title);
		contentValues.put(KEY_DESCRIPTION, description);
		contentValues.put(KEY_IMAGE, image);
		contentValues.put(KEY_VOTES, votes);
		contentValues.put(KEY_USER, user);
		contentValues.put(KEY_CATEGORY, category);
		return ourDatabase.insert(DATABASE_TABLE, null, contentValues);
		
	}

	public String getData() {
		// TODO Auto-generated method stub
		String[] columns = new String[]{ KEY_ID , KEY_TITLE, KEY_DESCRIPTION, KEY_IMAGE, KEY_VOTES, KEY_USER, KEY_CATEGORY };
		Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
		String result = "";
		
		int id = c.getColumnIndex(KEY_ID);
		int tit = c.getColumnIndex(KEY_TITLE);
		int des = c.getColumnIndex(KEY_DESCRIPTION);
		int ima = c.getColumnIndex(KEY_IMAGE);
		int vot = c.getColumnIndex(KEY_VOTES);
		int use = c.getColumnIndex(KEY_USER);
		int cat = c.getColumnIndex(KEY_CATEGORY);
		
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(id) + "..." + c.getString(tit) + "..." + c.getString(des) + "..." + c.getString(ima) + "..." + c.getString(vot) + "..." + c.getString(use) + "..." + c.getString(cat) + "\n ******************************** \n";
		}
		
		return result;
	}
	
	public void deleteTable () {
//		ourDatabase.delete(DATABASE_TABLE, null, null);
		ourDatabase.execSQL("DROP TABLE " + DATABASE_TABLE);
		ourDatabase.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" +
				KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				KEY_TITLE + " TEXT NOT NULL, " +
				KEY_DESCRIPTION + " TEXT NOT NULL, " + 
				KEY_IMAGE + " TEXT, " +
				KEY_VOTES + " INTEGER NOT NULL, " + 
				KEY_USER + " INTEGER NOT NULL," + 
				KEY_CATEGORY + " INTEGER NOT NULL);"
		);
	}
	
	
}

