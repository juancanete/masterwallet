package com.masternews.customizedlist;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.masternews.controllerdb.NewsController;
import com.project.masterwallet.Detalle;
import com.project.masterwallet.R;
import com.project.masterwallet.R.id;
import com.project.masterwallet.R.layout;
import com.project.singleton.UsuarioSingleton;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomizedListView extends Fragment {
	// All static variables
	static final String URL = "http://api.androidhive.info/music/music.xml";
	// XML node keys
	//public static final String KEY_SONG = "title"; // parent node
	public static final String KEY_ID = "id";
	public static final String KEY_TITLE = "title";
	public static final String KEY_DESC = "description";
	public static final String KEY_VOTES = "votes";
	public static final String KEY_THUMB_URL = "thumb_url";

	public static final String KEY_USER = "iduser";
	public static final String KEY_CAT = "idCategoria";

	private String idNew = "";
	private String idUsu = "";
	private String idCate = "";
	private int usu = 0;
	private int cate = 0;

	NewsController noticiaController;
	ListView list;
	ListRowDatas adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View lview = inflater.inflate(R.layout.mainlist, container, false);

		// Toast.makeText(lview.getContext(), "�" +
		// UsuarioSingleton.getSingletonInstance().getNombre() +
		// "is connected!", Toast.LENGTH_SHORT);

		// Load News List
		noticiaController = new NewsController();

		noticiaController.getNewsDESC();
		
		ArrayList<HashMap<String, String>> newsList = new ArrayList<HashMap<String, String>>();

		try {

			for (int i = 0; i < noticiaController.getArrJSONNews().size(); i++) {
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();
				JSONObject newJSON = noticiaController.getArrJSONNews().get(i);

				// adding each child node to HashMap key =&gt; value

				map.put(KEY_ID, String.valueOf(newJSON.getInt("idnoticia")));
				map.put(KEY_TITLE, newJSON.getString("titulo"));
				map.put(KEY_DESC, newJSON.getString("descripcion"));
				map.put(KEY_VOTES, newJSON.getString("votos"));
				map.put(KEY_THUMB_URL, newJSON.getString("imagen"));

				// adding HashList to ArrayList
				newsList.add(map);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		list = (ListView) lview.findViewById(R.id.list_news);

		// Getting adapter by passing xml data ArrayList
		adapter = new ListRowDatas(this.getActivity(), newsList);
		list.setAdapter(adapter);

		// Click event for single list row
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(view.getContext(), Detalle.class);

				// HashMap<String, String> newsData =
				// ((ListRowDatas)parent.getAdapter().getItem(position)).getData().get(position);

				// ((TextView)v.findViewById(R.id.LblTitulo))
				// .getText().toString();
				TextView txtTitle = (TextView) view.findViewById(R.id.title);
				TextView txtDesc = (TextView) view
						.findViewById(R.id.description);
				TextView txtVotes = (TextView) view.findViewById(R.id.votes);
				// ImageView
				// thumb_image=(ImageView)view.findViewById(R.id.list_image);

				// Get image and convert to byte
				ImageView thumb_image = (ImageView) view
						.findViewById(R.id.list_image);

				thumb_image.setDrawingCacheEnabled(true);

				thumb_image.buildDrawingCache();

				Bitmap bm = thumb_image.getDrawingCache();

				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();

				HashMap<String, String> map = new HashMap<String, String>();
				JSONObject newJSON = noticiaController.getArrJSONNews().get(
						position);
				JSONObject userJSON, cateJSON;

				System.out.println("El valor de la posicion es " + position);

				try {

					userJSON = newJSON.getJSONObject("idusuario");
					cateJSON = newJSON.getJSONObject("idcategoria");
					map.put(KEY_ID,
							idNew = String.valueOf(newJSON.getInt("idnoticia")));
					map.put(KEY_TITLE, newJSON.getString("titulo"));
					map.put(KEY_DESC, newJSON.getString("descripcion"));
					map.put(KEY_VOTES, newJSON.getString("votos"));
					map.put(KEY_THUMB_URL, newJSON.getString("imagen"));
					map.put(KEY_USER, userJSON.getString("idusuario"));
					map.put(KEY_CAT, cateJSON.getString("idcategoria"));

					// map.put(KEY_CAT, idCate =
					// String.valueOf(newJSON.getInt("idcategoria")));

					System.out.println("El valor de id es " + idNew);
					System.out.println("El valor de id de usuario es " + idUsu);
					intent.putExtra(CustomizedListView.KEY_ID, idNew);
					intent.putExtra(CustomizedListView.KEY_TITLE, txtTitle
							.getText().toString());
					intent.putExtra(CustomizedListView.KEY_DESC, txtDesc
							.getText().toString());
					intent.putExtra(CustomizedListView.KEY_VOTES, txtVotes
							.getText().toString());
					intent.putExtra(CustomizedListView.KEY_THUMB_URL, byteArray);
					intent.putExtra("googleimage", newJSON.getString("imagen"));
					intent.putExtra(CustomizedListView.KEY_USER,
							userJSON.getString("idusuario"));

					intent.putExtra(CustomizedListView.KEY_CAT,
							cateJSON.getString("idcategoria"));
					intent.putExtra("userjson", userJSON.getString("idusuario"));
					intent.putExtra("categjson",
							cateJSON.getString("idcategoria"));

					UsuarioSingleton.getSingletonInstance().setUserJSON(
							userJSON);
					UsuarioSingleton.getSingletonInstance().setCategJSON(
							cateJSON);

					startActivity(intent);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		return lview;
	}

	@Override
	public void onStart() {
		super.onStart();

	}
}
