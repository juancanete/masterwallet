package com.masternews.controllerdb;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;

import com.project.singleton.MainSingleton;
import com.project.singleton.UsuarioSingleton;

public class NewsController {

	private ArrayList<JSONObject> arrJSONNews;
	private int idNoticia;
	private String titulo;
	private String descripcion;
	private String imagen;
	private int votos;
	private String nombreprop;

	public NewsController() {

	}

	public void gettingNoticias() {

		HttpGet del = new HttpGet(MainSingleton.getSingletonInstance().getUrlRest()
				+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() +"noticia/606");
		del.setHeader("content-type", "application/json");

		try {
			HttpResponse resp = MainSingleton.getSingletonInstance()
					.getHttpClient().execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			JSONObject respJSON = new JSONObject(respStr);

			idNoticia = respJSON.getInt("idnoticia");
			titulo = respJSON.getString("titulo");
			descripcion = respJSON.getString("descripcion");
			imagen = respJSON.getString("imagen");
			votos = respJSON.getInt("votos");
			nombreprop = respJSON.getString("nombre");

			System.out.println("Titulo: " + titulo);

		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);

		}
	}

	public void gettingNoticesRange(int numNews) {
		HttpGet del = new HttpGet(MainSingleton.getSingletonInstance().getUrlRest()
				+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() + "noticia/0/"
				+ numNews);
		del.setHeader("content-type", "application/json");

		arrJSONNews = new ArrayList<JSONObject>();

		try {
			HttpResponse resp = MainSingleton.getSingletonInstance()
					.getHttpClient().execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			JSONArray arrJSON = new JSONArray(respStr);

			for (int i = 0; i < arrJSON.length(); i++) {
				arrJSONNews.add(arrJSON.getJSONObject(i));
			}

		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);
			// resul = false;
		}
	}

	public void insertingNewsIntoDB(Short idUserInsert, String token,
			String title, String description, String idcategory) {
		String strBytes = token;
		System.out.println("Token: " + token);
		String encoderBase64 = new String(Base64.encodeToString(
				strBytes.getBytes(), Base64.DEFAULT));

		HttpPost post;

		try {

			post = new HttpPost(MainSingleton.getSingletonInstance().getUrlRest()
					+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() + "noticia/"
					+ idUserInsert + "?token="
					+ URLEncoder.encode(encoderBase64, "UTF-8"));

			post.setHeader("content-type", "application/json");

			// Usuario
			JSONObject usuario = UserController.userJSONDB(idUserInsert);

			// Categoria
			JSONObject categoria = CategoryController.categoryJSONDB(new Short(
					idcategory));

			JSONObject dato = new JSONObject();
			dato.put("idnoticia", 3);
			dato.put("titulo", title);
			dato.put("descripcion", description);
			dato.put("votos", 0);
			dato.put("imagen", UsuarioSingleton.getSingletonInstance()
					.getImagen());
			dato.put("idusuario", usuario);
			dato.put("idcategoria", categoria);

			String jsonString = dato.toString();

			System.out.println("JSON: " + jsonString);

			StringEntity entity = new StringEntity(dato.toString(), HTTP.UTF_8);
			post.setEntity(entity);

			MainSingleton.getSingletonInstance().getHttpClient().execute(post);

		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);
		}
	}

	public void deletingNews(Short idUserDelete, Short idNew, String token) {

		String encoderBase64 = new String(Base64.encodeToString(
				token.getBytes(), Base64.DEFAULT));

		HttpDelete del;

		try {
			del = new HttpDelete(MainSingleton.getSingletonInstance().getUrlRest()
					+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() + "noticia/"
					+ idUserDelete + "?token="
					+ URLEncoder.encode(encoderBase64, "UTF-8") + "&idNew="
					+ idNew);
			MainSingleton.getSingletonInstance().getHttpClient().execute(del);

		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);
		}
	}

	// ORDEN DESCENDENTE NOTICIAS

	public void getNewsDESC() {
		JSONArray arrJSON = new JSONArray();
		//ArrayList<JSONObject> arrayLastNews = new ArrayList<JSONObject>();

		arrJSONNews = new ArrayList<JSONObject>();
		
		HttpGet del = new HttpGet(MainSingleton.getSingletonInstance().getUrlRest()
				+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() + "noticia/lastNews");

		del.setHeader("content-type", "application/json");
		HttpResponse resp;
		try {
			resp = MainSingleton.getSingletonInstance().getHttpClient()
					.execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			arrJSON = new JSONArray(respStr);

			for (int i = 0; i < arrJSON.length(); i++) {
				arrJSONNews.add(arrJSON.getJSONObject(i));
			}
		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);
			// resul = false;
		}
	}

	// DESTACADAS, ORDENADAS POR VOTOS

	public ArrayList<JSONObject> getNewsDestacadas() {
		JSONArray arrJSON = new JSONArray();
		ArrayList<JSONObject> arrayDestacadas = new ArrayList<JSONObject>();

		HttpGet del = new HttpGet(MainSingleton.getSingletonInstance().getUrlRest()
				+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() + "noticia/destacadas");

		del.setHeader("content-type", "application/json");
		HttpResponse resp;
		try {
			resp = MainSingleton.getSingletonInstance().getHttpClient()
					.execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			arrJSON = new JSONArray(respStr);

			for (int i = 0; i < arrJSON.length(); i++) {
				arrayDestacadas.add(arrJSON.getJSONObject(i));
			}
		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);
			// resul = false;
		}

		return arrayDestacadas;
	}

	// POR TIPO CATEGORÍA

	public void getNewsCategoriaJSONDB(Short idCategoria) {
		JSONArray arrJSON = new JSONArray();
		arrJSONNews = new ArrayList<JSONObject>();

		HttpGet del = new HttpGet(MainSingleton.getSingletonInstance().getUrlRest()
				+ ":" + MainSingleton.getSingletonInstance().getPort() + MainSingleton.getSingletonInstance().getUrlMethod() + "noticia/categoria/"
				+ idCategoria);

		del.setHeader("content-type", "application/json");
		HttpResponse resp;
		try {
			resp = MainSingleton.getSingletonInstance().getHttpClient()
					.execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());
			arrJSON = new JSONArray(respStr);

			for (int i = 0; i < respStr.length(); i++) {
				arrJSONNews.add(arrJSON.getJSONObject(i));
			}
		} catch (Exception ex) {
			Log.e("ServicioRest", "Error!", ex);
		}
	}

	public ArrayList<JSONObject> getArrJSONNews() {
		return arrJSONNews;
	}

	public void setArrJSONNews(ArrayList<JSONObject> arrJSONNews) {
		this.arrJSONNews = arrJSONNews;
	}
	
	

}
