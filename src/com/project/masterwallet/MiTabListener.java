package com.project.masterwallet;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ActionBar.Tab;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;

public class MiTabListener implements ActionBar.TabListener {

	private Fragment fragment;
	
	public MiTabListener(Fragment tab1frag)
	{
		this.fragment = tab1frag;
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	Log.i("ActionBar", tab.getText() + " reseleccionada.");
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		Log.i("ActionBar", tab.getText() + " seleccionada.");
		ft.replace(R.id.contenedor, fragment);
	}
	
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		Log.i("ActionBar", tab.getText() + " deseleccionada.");
		ft.remove(fragment);
	}

}






