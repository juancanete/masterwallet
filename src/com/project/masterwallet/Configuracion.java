package com.project.masterwallet;

import java.io.InputStream;

import com.project.singleton.UsuarioSingleton;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class Configuracion extends Activity {
	
	private static final int MNU_OPC1 = 1;
	private static final int MNU_OPC2 = 2;
	private static final int MNU_OPC3 = 3;
	private static final int MNU_OPC4 = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);
		
		//Conseguir el mensaje de intent
		Intent intent = getIntent();
		
		//Datos del usuario
		String nombre = UsuarioSingleton.getSingletonInstance().getNombre();
		String imagen = UsuarioSingleton.getSingletonInstance().getImagen();
		String email = UsuarioSingleton.getSingletonInstance().getEmail();
		
		ImageView fotoPerfil = (ImageView) findViewById(R.id.imagenperfil);
		new LoadProfileImage(fotoPerfil).execute(imagen);
		
		TextView nombreUsuario = (TextView) findViewById(R.id.nombreperfil);
		nombreUsuario.setText(nombre);

		TextView emailUsuario = (TextView) findViewById(R.id.emailperfil);
	    emailUsuario.setText(email);
	    
		setupActionBar();
	}

	private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
 
        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }
 
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }
 
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
	
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.configuracion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
//		switch (item.getItemId()) {
//		
//        case R.id.action_settings:
//        	Intent intent0 = new Intent(this, Configuracion.class);
//    		startActivity(intent0);
//            return true;
//        case R.id.deportes:
//    		Intent intent1 = new Intent(this, Deportes.class);
//    		startActivity(intent1);
//        	//fragment = new Deportes();
//           // lblMensaje.setText("Deportes");
//            return true;
//        case R.id.anuncios:
//    		Intent intent2 = new Intent(this, CategoryNews.class);
//    		startActivity(intent2);
//        	//lblMensaje.setText("Anuncios");;
//            return true;
//        case R.id.actualidad:
//    		Intent intent3 = new Intent(this, Actualidad.class);
//    		startActivity(intent3);
//        	//lblMensaje.setText("Actualidad");;
//            return true;
//        case R.id.cotilleos:
//        	Intent intent4 = new Intent(this, Cotilleos.class);
//    		startActivity(intent4);
//        	//lblMensaje.setText("Cotilleos");;
//            return true;
//        default:
//            return super.onOptionsItemSelected(item);
//		}
		return super.onOptionsItemSelected(item);
	}

}
