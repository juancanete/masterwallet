package com.project.masterwallet;

import com.masternews.controllerdb.TokenController;
import com.masternews.customizedlist.CustomizedListView;
import com.project.singleton.UsuarioSingleton;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class Principal extends Activity {

	private static final int MNU_OPC1 = 1;
	private static final int MNU_OPC2 = 2;
	private static final int MNU_OPC3 = 3;
	private static final int MNU_OPC4 = 4;

	private TextView lblMensaje;
	TokenController tokenController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);

		tokenController = new TokenController();

		lblMensaje = (TextView) findViewById(R.id.LblMensaje);

		// Conseguir el mensaje de intent
		Intent intent = getIntent();

		// Obtenemos una referencia a la actionbar
		final ActionBar actionBar = getActionBar();

		// Establecemos el modo de navegacion por pesta��as
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Mostramos el boton del actionbar
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Creamos las pesta��as
		ActionBar.Tab tab1 = actionBar.newTab().setText("Ultimas noticias");
		ActionBar.Tab tab2 = actionBar.newTab().setText("Destacadas");
		// ActionBar.Tab tab3 = actionBar.newTab().setText("Destacadas");
		// ActionBar.Tab tab4 = actionBar.newTab().setText("ListCustom");

		// Creamos los fragmentos de cada pesta��a
		Fragment tab1frag = new CustomizedListView();
		Fragment tab2frag = new Destacadas();
		// Fragment tab3frag = new Destacadas();
		// Fragment tab4frag = new CustomizedListView();

		// Asociamos los listener a las pesta��as
		tab1.setTabListener(new MiTabListener(tab1frag));
		tab2.setTabListener(new MiTabListener(tab2frag));

		// A��adimos las pesta��as a la action bar
		actionBar.addTab(tab1);
		actionBar.addTab(tab2);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent;
		short numCategory = 0;
		String nameCat = "";

		switch (item.getItemId()) {
		case R.id.deportes:
			numCategory = 2;
			nameCat = "Deportes";
			break;
		case R.id.anuncios:
			numCategory = 1;
			nameCat = "Anuncios";
			break;
		case R.id.actualidad:
			numCategory = 0;
			nameCat = "Actualidad";
			break;
		case R.id.cotilleos:
			numCategory = 3;
			nameCat = "Cotilleos";
			break;
		default:

		}

		if (item.getItemId() == R.id.menu_new) {
			intent = new Intent(this, Add.class);
			startActivity(intent);
			return true;
		} else if (item.getItemId() == R.id.action_settings) {
			intent = new Intent(this, Configuracion.class);
			startActivity(intent);
			return true;
		} else if ((item.getItemId() != R.id.menu_new)
				&& (item.getItemId() != R.id.action_settings)) {
			intent = new Intent(this, CategoryNews.class);
			intent.putExtra("category", numCategory);
			intent.putExtra("nameCategory", nameCat);
			startActivity(intent);
			return true;

		} else {
			return super.onOptionsItemSelected(item);
		}

	}
}
