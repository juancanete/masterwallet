package com.project.masterwallet;

import com.masternews.controllerdb.NewsController;
import com.project.singleton.UsuarioSingleton;

import android.R.string;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class Add extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	// SectionsPagerAdapter mSectionsPagerAdapter;

	String[] datos = { "","Actualidad","Anuncios","Deportes", "Cotilleos" };

	private static final int SELECT_PICTURE = 1;
	private Uri selectedImageUri;
	private NewsController noticiaController;
	private int categoryId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);

		noticiaController = new NewsController();
		
		Spinner categoria_prompt = (Spinner) findViewById(R.id.spinner1);

		ArrayAdapter<String> Adaptador = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, datos);
		categoria_prompt.setAdapter(Adaptador);

		categoria_prompt
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> adapterview,
							View view, int i, long l) {
						switch (i) {
						case 1:
							categoryId= 0;
							Toast to = Toast.makeText(getApplicationContext(),
									datos[i], Toast.LENGTH_LONG);
							to.show();
							break;
						case 2:
							categoryId= 1;
							Toast t = Toast.makeText(getApplicationContext(),
									datos[i], Toast.LENGTH_LONG);
							t.show();
							break;
						case 3:
							categoryId= 2;
							Toast ta = Toast.makeText(getApplicationContext(),
									datos[i], Toast.LENGTH_LONG);
							ta.show();
							break;
						case 4:
							categoryId= 3;
							Toast tu = Toast.makeText(getApplicationContext(),
									datos[i], Toast.LENGTH_LONG);
							tu.show();
							break;

						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		((Button) findViewById(R.id.btnBuscar))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {

						// in onCreate or any event where your want the user to
						// select a file
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(
								Intent.createChooser(intent, "Select Picture"),
								SELECT_PICTURE);
					}
				});
		
		
		((Button) findViewById(R.id.button_save))
		.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				EditText title = (EditText)findViewById(R.id.TxtTitulo);
				EditText description = (EditText)findViewById(R.id.TxtDescripcion);
				
				
				Short idUser = UsuarioSingleton.getSingletonInstance().getIdUsuario();
				noticiaController.insertingNewsIntoDB(idUser, MainActivity.token, title.getText().toString(), description.getText().toString(),String.valueOf(categoryId));
				
			}
		});

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				selectedImageUri = data.getData();
				// selectedImagePath = getPath(selectedImageUri);

				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImageUri,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				cursor.close();

				Bitmap bm = BitmapFactory.decodeFile(picturePath);
				// ImageView imagen=((ImageView) findViewById(R.id.imgView));

				Drawable verticalImage = new BitmapDrawable(getResources(), bm);
				ImageView imagen = (ImageView) findViewById(R.id.imgView);
				imagen.setImageDrawable(verticalImage);

				// Uri imageUri=data.getData();
				//
				// List<NameValuePair> params = new ArrayList<NameValuePair>(1);
				// params.add(new BasicNameValuePair("image",
				// imageUri.getPath()));
				// post("AQUI VA LA DIRECCION DEL SERVIDOR",params);
			}
		}
	}

	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);

		}
		// this is our fallback here

		return uri.getPath();

	}

}
