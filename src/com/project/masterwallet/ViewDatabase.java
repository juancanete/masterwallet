package com.project.masterwallet;

import com.masternews.innerdb.InternalStorageNoticias;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class ViewDatabase extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_database);
		
		TextView tv = (TextView) findViewById(R.id.viewInfo);
		InternalStorageNoticias info = new InternalStorageNoticias(this);
		info.open();
		String data = info.getData();
		info.close();
		tv.setText(data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_database, menu);
		return true;
	}

}
