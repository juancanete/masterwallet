package com.project.masterwallet;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.masternews.controllerdb.NewsController;
import com.masternews.customizedlist.CustomizedListView;
import com.masternews.customizedlist.ListRowDatas;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class CategoryNews extends Activity {
	
	// All static variables
	static final String URL = "http://api.androidhive.info/music/music.xml";
	// XML node keys
	static final String KEY_SONG = "song"; // parent node
	static final String KEY_ID = "id";
	static final String KEY_TITLE = "title";
	static final String KEY_ARTIST = "artist";
	static final String KEY_DURATION = "duration";
	static final String KEY_THUMB_URL = "thumb_url";

	protected NewsController noticiaController;
	protected ListView list;
	protected ListRowDatas adapter;
	protected String nameCat;
	short numCat;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_anuncios);
		
		//Conseguir el mensaje de intent
		Intent intent = getIntent();
		nameCat = intent.getStringExtra("nameCategory");
		numCat = intent.getShortExtra("category", (short) 0);
		setTitle(nameCat);
		
		// Show the Up button in the action bar.
		setupActionBar();
		
		noticiaController = new NewsController();
		noticiaController.getNewsCategoriaJSONDB(numCat);

		//System.out.println("COunt noticias: " + noticiaController.getNewsCategoriaJSONDB(numCat).size());
		ArrayList<HashMap<String, String>> newsList = new ArrayList<HashMap<String, String>>();

		try {

			for (int i = 0; i < noticiaController.getArrJSONNews().size(); i++) {
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();
				JSONObject newJSON = noticiaController.getArrJSONNews().get(i);

				// adding each child node to HashMap key =&gt; value

				map.put(KEY_ID, String.valueOf(newJSON.getInt("idnoticia")));
				map.put(KEY_TITLE, newJSON.getString("titulo"));
				map.put(KEY_ARTIST, newJSON.getString("descripcion"));
				map.put(KEY_DURATION, newJSON.getString("votos"));
				map.put(KEY_THUMB_URL, newJSON.getString("imagen"));

				// adding HashList to ArrayList
				newsList.add(map);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		list = (ListView) findViewById(R.id.list_news);

		// Getting adapter by passing xml data ArrayList
		adapter = new ListRowDatas(this, newsList);
		list.setAdapter(adapter);

		// Click event for single list row
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(view.getContext(), Detalle.class);
				
				TextView txtTitle = (TextView) view.findViewById(R.id.title);
				TextView txtDesc = (TextView) view.findViewById(R.id.description);
				TextView txtVotes = (TextView) view.findViewById(R.id.votes);
				//ImageView thumb_image=(ImageView)view.findViewById(R.id.list_image);
				
				
				//Get image and convert to byte
				ImageView thumb_image = (ImageView)view.findViewById(R.id.list_image);

				thumb_image.setDrawingCacheEnabled(true);

				thumb_image.buildDrawingCache();

				Bitmap bm = thumb_image.getDrawingCache();
				
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				
				intent.putExtra(CustomizedListView.KEY_TITLE, txtTitle.getText().toString());
				intent.putExtra(CustomizedListView.KEY_DESC, txtDesc.getText().toString());
				intent.putExtra(CustomizedListView.KEY_VOTES, txtVotes.getText().toString());
				intent.putExtra(CustomizedListView.KEY_THUMB_URL, byteArray);
				startActivity(intent);
			}
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.anuncios, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		return super.onOptionsItemSelected(item);
	}
	
}
