package com.project.masterwallet;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.google.android.gms.maps.model.Tile;
import com.masternews.customizedlist.CustomizedListView;
import com.project.singleton.MainSingleton;
import com.project.singleton.UsuarioSingleton;

import android.os.Bundle;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class Detalle extends Activity {

	private static final int MNU_OPC1 = 1;
	private static final int MNU_OPC2 = 2;
	private static final int MNU_OPC3 = 3;
	private static final int MNU_OPC4 = 4;

	HttpClient httpClient = new DefaultHttpClient();
	private Button votar;
	private int intVotos = 0;
	//private int idNoticia = restnoticia.getIdNoticia();
	
	private String shareBody = "";
	private String shareImage = "";
	private String shareTitle = "";
	private String textVotos = "";
	private String idText = "";
	private String TextUsu = "";
	private String TextCat = "";
	private Short idNew = 0;
	private Short idUsu = 0;
	private Short idCat = 0;
	
	
	
	HttpPut put;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle);

		// Conseguir el mensaje de intent
		Intent intent = getIntent();
		TextView title = (TextView) findViewById(R.id.title_new);
		TextView description = (TextView) findViewById(R.id.title_description);
		TextView votos = (TextView) findViewById(R.id.detail_votos);

		byte[] image = intent.getByteArrayExtra(CustomizedListView.KEY_THUMB_URL);
		
		Bitmap bmp=BitmapFactory.decodeByteArray(image,0,image.length);
		ImageView imageView= (ImageView) findViewById(R.id.detail_image);
		imageView.setImageBitmap(bmp);
		
		
		title.setText(intent.getStringExtra(CustomizedListView.KEY_TITLE));
		description.setText(intent.getStringExtra(CustomizedListView.KEY_DESC));
		votos.setText(intent.getStringExtra(CustomizedListView.KEY_VOTES));
		
		idText = intent.getStringExtra(CustomizedListView.KEY_ID);
		System.out.println("El valor de id es " + idText);
		
		idNew = new Short(idText);
		System.out.println("El valor de id es " + idNew);
		//idNew = idN;
		
		TextUsu = intent.getStringExtra(CustomizedListView.KEY_USER);
		idUsu = new Short(TextUsu);
		System.out.println("El id de usuario es: " + idUsu);
		
		TextCat = intent.getStringExtra(CustomizedListView.KEY_CAT);
		idCat = new Short(TextCat);
		System.out.println("El id de categoria es: " + idCat);
		
		shareBody = description.getText().toString();
		shareTitle = title.getText().toString();
		shareImage = intent.getStringExtra("googleimage");
		
		textVotos = votos.getText().toString();
		intVotos = new Integer(textVotos).intValue();
//		userJSON = (JSONObject) intent.getSerializableExtra("userjson");
//		categJSON = (JSONObject) intent.getSerializableExtra("categjson");
		
		//intVotos = iVotos;
			
		// Boton para votar la noticia

		votar = (Button) findViewById(R.id.Bvotar);

		votar.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				intVotos = intVotos + 1;
				//System.out.println("El numero de votos es " + intVotos);
				TextView votos = (TextView) findViewById(R.id.detail_votos);
				votos.setText(String.valueOf(intVotos));
				
				try {
					
					put = new HttpPut("http://" + MainSingleton.getSingletonInstance().getUrlRest()
							+ ":8080/AndroidRest/webresources/entities.noticia/"+ idNew);
					put.setHeader("content-type", "application/json");
				
					JSONObject dato = new JSONObject();
					dato.put("idnoticia", idNew);
					dato.put("titulo", shareTitle);
					dato.put("descripcion", shareBody);
					dato.put("votos", intVotos);
					dato.put("imagen", shareImage);
					dato.put("idusuario", UsuarioSingleton.getSingletonInstance().getUserJSON());
					dato.put("idcategoria", UsuarioSingleton.getSingletonInstance().getCategJSON());
					
					String jsonString = dato.toString();

					System.out.println("JSON: " + jsonString);

					StringEntity entity = new StringEntity(dato.toString(), HTTP.UTF_8);
					put.setEntity(entity);

					HttpResponse resp = httpClient.execute(put);
					String respStr = EntityUtils.toString(resp.getEntity());
					Log.e("Servicio Rest", respStr);
				} catch (Exception ex) {
					Log.e("ServicioRest", "Error!", ex);
				}
				
				//restnoticia.insertVotesIntoDB(idNew, intVotos, shareTitle, shareBody);
				
				TextView textView = (TextView) findViewById(R.id.vot);
				textView.setText("Esta aplicacion tiene " + intVotos + " votos");
			}
		});

		// Boton para compartir en facebook
		ImageButton compartirButton = (ImageButton) findViewById(R.id.Bface);

		compartirButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				shareIt();
			}
		});

		setupActionBar();
	}

	private void shareIt() {

		Intent compartir = new Intent(android.content.Intent.ACTION_SEND);
		compartir.setType("text/plain");
		compartir.putExtra(android.content.Intent.EXTRA_SUBJECT, shareTitle);
		compartir.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(compartir,
				"Elige via para compartir"));

	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detalle, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case R.id.action_settings:
//			Intent intent0 = new Intent(this, Configuracion.class);
//			startActivity(intent0);
//			return true;
//		case R.id.deportes:
//			Intent intent1 = new Intent(this, Deportes.class);
//			startActivity(intent1);
//			// fragment = new Deportes();
//			// lblMensaje.setText("Deportes");
//			return true;
//		case R.id.anuncios:
//			Intent intent2 = new Intent(this, CategoryNews.class);
//			startActivity(intent2);
//			// lblMensaje.setText("Anuncios");;
//			return true;
//		case R.id.actualidad:
//			Intent intent3 = new Intent(this, Actualidad.class);
//			startActivity(intent3);
//			// lblMensaje.setText("Actualidad");;
//			return true;
//		case R.id.cotilleos:
//			Intent intent4 = new Intent(this, Cotilleos.class);
//			startActivity(intent4);
//			// lblMensaje.setText("Cotilleos");;
//			return true;
//		default:
//			return super.onOptionsItemSelected(item);
//		}
		return super.onOptionsItemSelected(item);
	}

}
