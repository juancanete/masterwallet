package com.project.singleton;

import org.json.JSONObject;

public class UsuarioSingleton {

	private static UsuarioSingleton instanceUsuario;
	protected String email;
	protected String nombre;
	protected Short idUsuario;
	protected String imagen;
	
	private JSONObject userJSON;
	private JSONObject categJSON;
	
	private UsuarioSingleton () {
		
	}
	
	public static UsuarioSingleton getSingletonInstance () {
		if (instanceUsuario == null){
			instanceUsuario = new UsuarioSingleton();
		}
		return instanceUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Short getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Short idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public JSONObject getUserJSON() {
		return userJSON;
	}

	public void setUserJSON(JSONObject userJSON) {
		this.userJSON = userJSON;
	}

	public JSONObject getCategJSON() {
		return categJSON;
	}

	public void setCategJSON(JSONObject categJSON) {
		this.categJSON = categJSON;
	}
	
	
	
}
